package com.part.collegepart.user.ui.profiletabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.part.collegepart.databinding.FragmentFavoritsBinding
import com.part.collegepart.user.ui.ArticleAdapter


class FavoritsFragment : Fragment() {

    lateinit var binding: FragmentFavoritsBinding
    private val viewModel by viewModels<PostsViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentFavoritsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.postsResult.observe(viewLifecycleOwner) { list ->
            binding.rvFavoritPost.adapter = ArticleAdapter(requireContext(), list)
        }

    }


}