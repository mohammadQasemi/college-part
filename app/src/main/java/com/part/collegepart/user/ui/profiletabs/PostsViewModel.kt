package com.part.collegepart.user.ui.profiletabs

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.part.collegepart.user.ui.Article

class PostsViewModel : ViewModel() {

    val postsResult = MutableLiveData<List<Article>>()

    val repository = PostsRepository()

    init {
        getPosts()
    }

    private fun getPosts() {
        val post = repository.getPosts()
        postsResult.postValue(post)
    }

}