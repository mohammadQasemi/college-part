package com.part.collegepart.user.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.part.collegepart.R

class ArticleAdapter(val context: Context, private val dataSet: List<Article>) :
    RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.tv_name)
        val title: TextView = view.findViewById(R.id.tv_title)
        val like: ImageView = view.findViewById(R.id.tv_like)
        val likecount: TextView = view.findViewById(R.id.tv_like_count)
        val commentcount: TextView = view.findViewById(R.id.tv_comment_count)
        val time: TextView = view.findViewById(R.id.tv_time)
        val detail: TextView = view.findViewById(R.id.tv_detail)
        val profile: ImageView = view.findViewById(R.id.iv_profile)
        val bookmark: ImageView = view.findViewById(R.id.iv_bookmark)

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.row_article, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.name.text = dataSet[position].name
        viewHolder.title.text = dataSet[position].title
        viewHolder.time.text = dataSet[position].time
        viewHolder.detail.text = dataSet[position].detail
        viewHolder.likecount.text = dataSet[position].likeCount
        viewHolder.commentcount.text = dataSet[position].commentCount
        viewHolder.profile.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                dataSet[position].profile
            )
        )
        if (dataSet[position].bookmark) {
            viewHolder.bookmark.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_bookmark_border_colored
                )
            )
        } else
            viewHolder.bookmark.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_bookmark_border_white
                )
            )
        if (dataSet[position].like) {
            viewHolder.like.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_like
                )
            )
        } else
            viewHolder.like.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_favorited
                )
            )
    }

    override fun getItemCount() = dataSet.size

}
