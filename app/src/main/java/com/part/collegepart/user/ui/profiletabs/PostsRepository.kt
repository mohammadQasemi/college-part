package com.part.collegepart.user.ui.profiletabs

import com.part.collegepart.user.ui.Article

class PostsRepository {

    private val postslocaldatasource = PostsLocalDataSource()

    fun getPosts(): List<Article> =
        postslocaldatasource.getPosts()


}