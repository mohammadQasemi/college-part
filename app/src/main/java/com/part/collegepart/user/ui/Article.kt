package com.part.collegepart.user.ui

data class Article(
    val name: String,
    val bookmark: Boolean,
    val title: String,
    val detail: String,
    val time: String,
    val profile: Int,
    val commentCount: String,
    val likeCount: String,
    val like: Boolean
)
