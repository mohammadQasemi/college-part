package com.part.collegepart.user.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.part.collegepart.R
import com.part.collegepart.databinding.FragmentProfileBinding
import com.part.collegepart.user.ui.profiletabs.FavoritsFragment
import com.part.collegepart.user.ui.profiletabs.PostsFragment


class ProfileFragment : Fragment() {
    lateinit var binding: FragmentProfileBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewpager.adapter = TabsAdapter(this)
        binding.back.setOnClickListener {
            findNavController().navigateUp()
        }
        binding.btnFollow.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_postDetailFragment)
        }
        tuning()

    }

    private fun tuning() {
        TabLayoutMediator(binding.appbar, binding.viewpager) { tab, position ->
            when (position) {
                0 -> tab.text = "نوشته ها"
                1 -> tab.text = "علاقمندی ها"
            }
        }.attach()

    }
}

class TabsAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> PostsFragment()
            1 -> FavoritsFragment()
            else -> PostsFragment()
        }

    }
}



