package com.part.collegepart.home.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.part.collegepart.R
import com.part.collegepart.databinding.FragmentHomeBinding
import com.part.collegepart.home.ui.tab.*


class HomeFragment : Fragment() {
    lateinit var binding: FragmentHomeBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.pager.adapter = TabAdapter(this)
        super.onViewCreated(view, savedInstanceState)

        binding.article.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_articleFragment)
        }

        binding.profile.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
        }
        setup()
    }

    private fun setup() {
        TabLayoutMediator(binding.tab, binding.pager) { tab, position ->
            when (position) {
                0 -> tab.text = "برای شما"
                1 -> tab.text = "بورس"
                2 -> tab.text = "سهام"
                3 -> tab.text = "اقتصاد"
                4 -> tab.text = "سرمایه گذاری"
                5 -> tab.text = "مالی"
            }
        }.attach()
    }
}

class TabAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return 6
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ForYouFragment()
            1 -> ExchangeFragment()
            2 -> StocksFragment()
            3 -> EconomyFragment()
            4 -> FinanceFragment()
            5 -> FinancialFragment()
            else -> ForYouFragment()
        }

    }

}
