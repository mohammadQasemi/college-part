package com.part.collegepart.home.ui.tab

import com.part.collegepart.home.ui.Post

class ForYouRepository {

    private val forYouLocalDataSource = ForYouLocalDataSource()

    private val forYouRemoteDataSource = ForYouRemoteDataSource()

    fun getShortPost(): List<Post> = forYouLocalDataSource.getShortPost()

    fun getRelatedPost(): List<Post> = forYouLocalDataSource.getRelatedPost()

    fun getUsersList(page: Int) = forYouRemoteDataSource.getUsersList(page)

}
