package com.part.collegepart.home.ui.tab

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.part.collegepart.User
import com.part.collegepart.core.resource.Status
import com.part.collegepart.home.ui.Post

class ForYouViewModel : ViewModel() {

    val shortPostResult = MutableLiveData<List<Post>>()
    val relatedPostResult = MutableLiveData<List<Post>>()
    val usersListResult = MutableLiveData<List<User>>()

    private val repository = ForYouRepository()

    init {
        getShortPost()
        getRelatedPost()
        getUsersList(1)
    }

    private fun getUsersList(page: Int) {
        val usersList = repository.getUsersList(page)
        if (usersList.status == Status.SUCCESS) {
            usersListResult.postValue(usersList.data?.data)
        } else {
            val s = 1
        }
    }

    private fun getShortPost() {

        val posts = repository.getShortPost()
        shortPostResult.postValue(posts)

    }

    private fun getRelatedPost() {

        val relatedPost = repository.getRelatedPost()
        relatedPostResult.postValue(relatedPost)

    }

}