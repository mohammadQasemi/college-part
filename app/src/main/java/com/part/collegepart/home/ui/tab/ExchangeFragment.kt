package com.part.collegepart.home.ui.tab

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.part.collegepart.User
import com.part.collegepart.databinding.FragmentExchangeBinding
import com.part.collegepart.user.UserAdapter

class ExchangeFragment : Fragment() {

    lateinit var binding: FragmentExchangeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentExchangeBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val postSample = mutableListOf<User>()
        for (i in 0 until 5) {
            postSample.add(
                (User(
                    first_name = "Mohammad",
                    last_name = "Ghasemi",
                    email = "Mohammad214766@gmail.com",
                    id = 1,
                    avatar = "",
                )
                        )
            )
        }

        binding.rvPostSample.adapter = UserAdapter(requireContext(), postSample)


    }

}