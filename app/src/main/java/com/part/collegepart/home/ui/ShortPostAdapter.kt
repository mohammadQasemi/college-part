package com.part.collegepart.home.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.part.collegepart.R

class ShortPostAdapter(val context: Context, private val dataSet: List<Post>) :
    RecyclerView.Adapter<ShortPostAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.tv_name)
        val time: TextView = view.findViewById(R.id.tv_time)
        val detail: TextView = view.findViewById(R.id.tv_detail)
        val profile: ImageView = view.findViewById(R.id.iv_profile)
        val bookmark: ImageView = view.findViewById(R.id.iv_bookmark)

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.row_short_post, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.name.text = dataSet[position].name
        viewHolder.time.text = dataSet[position].time
        viewHolder.detail.text = dataSet[position].detail
        viewHolder.profile.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                dataSet[position].profile
            )
        )
        if (dataSet[position].bookmark) {
            viewHolder.bookmark.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_bookmark_border_colored
                )
            )
        } else
            viewHolder.bookmark.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_bookmark_border_white
                )
            )
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}
