package com.part.collegepart.home.ui.tab

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.part.collegepart.databinding.FragmentForYouBinding
import com.part.collegepart.home.ui.PostAdapter
import com.part.collegepart.home.ui.ShortPostAdapter

class ForYouFragment : Fragment() {

    lateinit var binding: FragmentForYouBinding
    private val viewModel by viewModels<ForYouViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentForYouBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.shortPostResult.observe(viewLifecycleOwner) { posts ->
            binding.rvHome.adapter = ShortPostAdapter(requireContext(), posts)
        }

        viewModel.relatedPostResult.observe(viewLifecycleOwner) { relatedPosts ->
            binding.rvRelatedArticle.adapter = PostAdapter(requireContext(), relatedPosts)
        }

        viewModel.usersListResult.observe(viewLifecycleOwner) {
            Log.d("mmd", "onResponse: ${it[0].first_name}")
        }
    }
}