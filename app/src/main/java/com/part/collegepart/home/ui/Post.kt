package com.part.collegepart.home.ui

data class Post(
    val name: String,
    val bookmark: Boolean,
    val detail: String,
    val time: String,
    val profile: Int
)
