package com.part.collegepart.home.ui.tab

import com.part.collegepart.R
import com.part.collegepart.home.ui.Post

class ForYouLocalDataSource {


    fun getShortPost(): List<Post> {
        val posts = mutableListOf<Post>()
        for (i in 0 until 5) {
            posts.add(
                Post(
                    name = "jai",
                    bookmark = true,
                    detail = "to bayad bedani javad boz ast",
                    time = "3 day",
                    profile = R.drawable.pro,
                )
            )
        }
        return posts
    }

    fun getRelatedPost(): List<Post> {
        val relatedPost = mutableListOf<Post>()
        for (i in 0 until 5) {
            relatedPost.add(
                Post(
                    name = "jai",
                    bookmark = true,
                    detail = "to bayad bedani javad boz ast",
                    time = "3 day",
                    profile = R.drawable.profile,
                )
            )
        }
        return relatedPost
    }

}