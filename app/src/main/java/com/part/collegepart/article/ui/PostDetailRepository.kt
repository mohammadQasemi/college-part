package com.part.collegepart.article.ui

import com.part.collegepart.home.ui.Post

class PostDetailRepository {
    private val postDetailLocalDataSource = PostDetailLocalDataSource()

    fun getPostDetail(): List<Post> =
        postDetailLocalDataSource.getPostDetail()

    fun getRelated(): List<Comment> =
        postDetailLocalDataSource.getRelated()


}