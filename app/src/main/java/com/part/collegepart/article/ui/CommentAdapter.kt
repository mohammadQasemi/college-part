package com.part.collegepart.article.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.part.collegepart.R

class CommentAdapter(val context: Context, private val comments: List<Comment>) :
    RecyclerView.Adapter<CommentAdapter.ViewHolder>() {


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.tv_name)
        val detail: TextView = view.findViewById(R.id.tv_detail)
        val profile: ImageView = view.findViewById(R.id.iv_profile)

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.comment_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.name.text = comments[position].name
        viewHolder.detail.text = comments[position].detail
        viewHolder.profile.setImageDrawable(
            ContextCompat.getDrawable(
                context,
                comments[position].profile
            )
        )

    }

    override fun getItemCount() = comments.size
}