package com.part.collegepart.article.ui

data class Comment(
    val name: String,
    val detail: String,
    val profile: Int
)


