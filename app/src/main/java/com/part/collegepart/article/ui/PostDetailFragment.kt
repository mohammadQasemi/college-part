package com.part.collegepart.article.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.part.collegepart.databinding.FragmentPostDetailBinding
import com.part.collegepart.home.ui.PostAdapter


class PostDetailFragment : Fragment() {

    lateinit var binding: FragmentPostDetailBinding
    private val viewModel by viewModels<PostDetailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPostDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.ivBack.setOnClickListener {
            findNavController().navigateUp()
        }



        viewModel.postDetailResult.observe(viewLifecycleOwner) { list ->
            binding.rvRelatedArticle.adapter = PostAdapter(requireContext(), list)
        }


        viewModel.relatedResult.observe(viewLifecycleOwner) { list ->
            binding.rvComments.adapter = CommentAdapter(requireContext(), list)
        }

    }

}




