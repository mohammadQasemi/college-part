package com.part.collegepart.article.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.chip.Chip
import com.part.collegepart.databinding.FragmentArticleBinding


class WriteArticleFragment : Fragment() {

    lateinit var binding: FragmentArticleBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentArticleBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.ivBack.setOnClickListener {
            findNavController().navigateUp()
        }
        binding.mainTagAutoCompleteTextView.addTextChangedListener(textWatcher)
        binding.mainTagAutoCompleteTextView.setOnEditorActionListener { v, actionId, event ->

            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val text = v.text
                addChip(text.toString())
                v.text = null
            }
            true
        }
    }

    private val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (s != null && s.isNotEmpty() && s.last() == ' ') {
                addChip(text = binding.mainTagAutoCompleteTextView.text.toString())
                binding.mainTagAutoCompleteTextView.text.clear()
            }
        }


        override fun afterTextChanged(s: Editable?) {

        }

    }

    fun addChip(text: String) {
        val chip = Chip(context)
        chip.text = text
        binding.chipGroup.addView(chip)
    }

}
