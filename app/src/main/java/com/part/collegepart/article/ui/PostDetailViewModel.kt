package com.part.collegepart.article.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.part.collegepart.home.ui.Post

class PostDetailViewModel : ViewModel() {

    val postDetailResult = MutableLiveData<List<Post>>()
    val relatedResult = MutableLiveData<List<Comment>>()

    private val repository = PostDetailRepository()


    init {
        getPostDetail()
        getRelated()
    }

    private fun getPostDetail() {
        val data = repository.getPostDetail()
        postDetailResult.postValue(data)
    }

    private fun getRelated() {
        val comments = repository.getRelated()
        relatedResult.postValue(comments)

    }

}