package com.part.collegepart.core

import com.part.collegepart.core.resource.Resource
import retrofit2.HttpException


inline fun <T> safeApiCall(responseFunction: () -> T): Resource<T> {
    return try {
        Resource.success(responseFunction.invoke())

    } catch (e: HttpException) {
        when {
            e.code() == 404 -> Resource.error("مشکل در ارتباط با سرور", null)
            e.code() == 402 -> Resource.error("اطلاعات اشتباه.", null)
            e.code() == 422 -> Resource.error("اطلاعات اشتباه.", null)
            else -> Resource.error(e.message.orEmpty(), null)
        }

    } catch (e: Exception) {
        Resource.error("مشکل در ارتباط با سرور", null)
    }
}
